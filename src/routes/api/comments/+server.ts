import { initDB } from "$lib/db";

export async function GET(event) {
    const db = initDB();
    const parent_id = new URL(event.request.url).searchParams.get("parent_id");
    let sql = `
        SELECT
            id,
            parent_id,
            body,
            STRFTIME('%s', created_at) as timestamp
        FROM comments
        WHERE active = 1
    `;

    const params = [];

    if (parent_id) {
        sql += ` AND parent_id = ?`;
        params.push(parent_id);
    } else {
        sql += ` AND parent_id IS NULL`;
    }

    sql += ` ORDER BY id DESC`;

    const comments = db.prepare(sql).all(...params);

    const resp = {
        "success": true,
        "comments": comments
    };

    return new Response(JSON.stringify(resp), {
        headers: {
            "content-type": "application/json"
        }
    });
}