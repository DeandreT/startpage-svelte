import { initDB } from "$lib/db";


export async function POST(event) {
    // Grab the body and parent_id from the request
    const { body, parent_id } = await event.request.json();
    if(!body || body.toString().trim() === "") {
        return new Response(JSON.stringify({
            error: "Missing comment body"
        }), {
            headers: {
                "content-type": "application/json"
            }
        });
    }
    const db = initDB();
    // Validate the parent_id
    if (parent_id) {
        const parent = db.prepare(`
            SELECT id
            FROM comments
            WHERE id = ?
        `).get(parent_id);
        if (!parent) {
            return new Response(JSON.stringify({
                error: "Invalid reply parent"
            }), {
                headers: {
                    "content-type": "application/json"
                }
            });
        }
    }
    // Insert a comment
    db.run(`
        INSERT INTO comments
        (body, parent_id, ip)
        VALUES (?, ?, ?)`,
        body,
        parent_id,
        event.getClientAddress()
    );

    const resp = {"success": true, "message": "Comment added"};
    return new Response(JSON.stringify(resp), {
        headers: {
            "content-type": "application/json"
        }
    });
}