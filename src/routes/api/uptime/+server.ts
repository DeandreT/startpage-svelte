import os from "os"

export async function GET(event) {
    const uptime = os.uptime()

    return new Response(JSON.stringify({
        "uptime": uptime,
        "server": "home"
    }), {
        headers: {
            "content-type": "application/json"
        }
    })
}