type Link = {
    url: string;
    fa_icon: string;
}

type PgComment = {
    id: number;
    ip: string;
    body: string;
    timestamp: number;
}

declare global {
    namespace App {
        interface PageData {
            links: Link[];
            comments: PgComment[];
        }
    }
}

