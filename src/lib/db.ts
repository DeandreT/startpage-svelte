import { Database } from "bun:sqlite";

export function initDB() {
    const db = new Database("comments.sqlite");
    db.exec(`
        CREATE TABLE IF NOT EXISTS comments (
            id INTEGER PRIMARY KEY,
            parent_id INTEGER DEFAULT NULL,
            ip TEXT,
            body TEXT,
            active INTEGER DEFAULT 1,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )
    `);
    return db;
}